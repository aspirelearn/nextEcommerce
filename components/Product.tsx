import { StaticImageData } from "@/node_modules/next/image"
import styles from '../styles/Product.module.scss';
import Image from 'next/image'

export interface IProduct {
    id: string
    name: string
    price: number
    url: string
    description: string
    image: StaticImageData
}

interface IProductProps {
    product: IProduct
}

import React from 'react'
import Link from "next/link";

export default function Product(props: IProductProps) {
  return (
    <div>
        <div className={styles.product}>
            <h2 className={styles.product__title}>{props.product.name}</h2>
            <p className={styles.product__description}>{props.product.description}</p>
            <div className={styles.product__image}>
                {/* <p>{props.product.image.src}</p> */}
                
            {/* <Image src={props.product.image.src}  /> */}
            <Image  src={props.product.image.src} alt='Dynamic Image' layout='responsive' width="350" height="300"/>
            </div>
            <div className="product__price-button-container">
                <div className={styles.product__price}>${props.product.price.toFixed(2)}</div>
                <button
                    className={`snipcart-add-item ${styles.product__button}`}
                    data-item-id={props.product.id}
                    data-item-name={props.product.name}
                    data-item-price={props.product.price}
                    data-item-url={props.product.url}
                    data-item-image={props.product.image.src}>
                    Add to cart
                </button>
                {/* <button className={`snipcart-add-item ${styles.product__button}`}> */}
                   
                    <Link 
                    href={{
                        pathname: '/products/'+props.product.id,
                        query: props.product.id // the data
                      }}
                    >
                    View Details 
								</Link>
                {/* </button> */}
            </div>
        </div>
    </div>
  )
}